package dd.mud.service.chat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DdMudChatServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DdMudChatServiceApplication.class, args);
	}
}
