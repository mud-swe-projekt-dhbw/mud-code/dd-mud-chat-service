package dd.mud.service.chat.boundary.consumer;

import dd.mud.service.chat.control.ChatMessageAggregator;
import dd.mud.service.chat.boundary.model.MudKafkaMessage;
import dd.mud.service.chat.boundary.repository.KafkaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ChatConsumer {

    private final ChatMessageAggregator chatMessageAggregator;

    /**
     * Receives the chat messages from the CWS.
     * @param mudKafkaMessage The chat message received from the CWS.
     */
    @KafkaListener(topics = KafkaRepository.CHAT_MESSAGE_TOPIC, groupId = "chatServiceGroup",
            containerFactory = "chatMessageFactory")
    public void receiveChatMessage(MudKafkaMessage mudKafkaMessage) {
        this.chatMessageAggregator.processChatMessage(mudKafkaMessage);
    }
}
