package dd.mud.service.chat.boundary.consumer;

import dd.mud.service.chat.control.GameUpdateAggregator;
import dd.mud.service.chat.boundary.model.CompactGame;
import dd.mud.service.chat.boundary.repository.KafkaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class GameUpdateKafkaConsumer {

    private final GameUpdateAggregator gameUpdateAggregator;

    /**
     * Receives the current game.
     * @param compactGame The current game as CompactGame
     */
    @KafkaListener(topics = KafkaRepository.REFRESH_GAME_TOPIC, groupId = "chatServiceGroup",
    containerFactory = "gameUpdateFactory")
    public void getCurrentGameFromGameService (CompactGame compactGame) {
        this.gameUpdateAggregator.processGameUpdate(compactGame);
    }
}