package dd.mud.service.chat.boundary.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChatServiceAnswer {
    private Long senderId;
    private String senderName;
    private List<Long> recipients;
    private String message;
}
