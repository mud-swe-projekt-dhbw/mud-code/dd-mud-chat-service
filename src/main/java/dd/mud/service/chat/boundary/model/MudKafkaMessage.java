package dd.mud.service.chat.boundary.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MudKafkaMessage {
    Long id;
    String message;
    Long gameId;
}
