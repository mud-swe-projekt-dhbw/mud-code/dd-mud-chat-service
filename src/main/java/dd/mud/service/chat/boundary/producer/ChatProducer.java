package dd.mud.service.chat.boundary.producer;

import dd.mud.service.chat.boundary.model.ChatServiceAnswer;
import dd.mud.service.chat.boundary.repository.KafkaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ChatProducer {

    private static final String SEND_TO_CWS_TOPIC = "chatAnswer.t";

    private final KafkaTemplate<String, ChatServiceAnswer> kafkaTemplate;

    /**
     * Sends the message to client websocket service.
     * @param chatServiceAnswer The message to send.
     */
    public void sendToClientWebsocket (ChatServiceAnswer chatServiceAnswer) {
        this.kafkaTemplate.send(SEND_TO_CWS_TOPIC, chatServiceAnswer);
    }
}
