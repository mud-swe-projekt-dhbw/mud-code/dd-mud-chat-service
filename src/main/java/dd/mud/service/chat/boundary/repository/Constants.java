package dd.mud.service.chat.boundary.repository;

public final class Constants {
    public static final String AT = "@";
    public static final String COLON = ":";
    public static final String WHITESPACE = " ";

    public static final String DM_TAG = "[DM] ";
    public static final String MESSAGE_FROM_DM = "DM";

    public static final String ERROR_MESSAGE_INVALID_SYNTAX =
            "Die zuletzt versendete private Nachricht entspricht nicht der geforderten Syntax: " +
                    "@benutzername1@benutzername2: nachricht";
    public static final String ERROR_MESSAGE_UNKNOWN_USERNAME =
            "Die zuletzt versendete private Nachricht enthält einen unbekannten Empfänger";
}
