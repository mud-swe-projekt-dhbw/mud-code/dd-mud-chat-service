package dd.mud.service.chat.boundary.repository;

public final class KafkaRepository {
    public static final String CHAT_MESSAGE_TOPIC = "chatChatMessage.t";
    public static final String REFRESH_GAME_TOPIC = "chatCurrentGame.t";
    public static final String CHAT_CONSUMER_GROUP_ID = "chatConsumerGroup";
    public static final String GAME_CONSUMER_GROUP_ID = "chatConsumerGroup";
}
