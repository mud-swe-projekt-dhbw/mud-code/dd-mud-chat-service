package dd.mud.service.chat.control;

import dd.mud.service.chat.boundary.producer.ChatProducer;
import dd.mud.service.chat.control.services.MessageParser;
import dd.mud.service.chat.control.services.RecipientsInvestigatorService;
import dd.mud.service.chat.boundary.model.ChatServiceAnswer;
import dd.mud.service.chat.boundary.model.MudKafkaMessage;
import dd.mud.service.chat.boundary.repository.Constants;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ChatMessageAggregator {

    private final RecipientsInvestigatorService recipientsInvestigatorService;
    private final ChatProducer chatProducer;
    private final MessageParser messageParser;

    /**
     * Retrieves the recipients and builds a chat answer to be sent to the CWS.
     * @param mudKafkaMessage The chat message received from the chat service.
     */
    public void processChatMessage (MudKafkaMessage mudKafkaMessage) {

        ChatServiceAnswer chatServiceAnswer;
        String[] messageString = mudKafkaMessage.getMessage().split("[:]");
        String senderName = messageString[0];
        String realMessage = getMessageWithoutSender(messageString);

        MudKafkaMessage realMudKafkaMessage = MudKafkaMessage.builder()
                .gameId(mudKafkaMessage.getGameId())
                .id(mudKafkaMessage.getId())
                .message(realMessage)
                .build();

        if(realMudKafkaMessage.getMessage().startsWith(Constants.AT)) {
            chatServiceAnswer = messageParser.buildChatMessages(realMudKafkaMessage, senderName);
        }
        else {
            String message = messageParser.addDmTagIfNecessary(mudKafkaMessage.getId()
                    , mudKafkaMessage.getGameId(), realMessage);
            chatServiceAnswer = ChatServiceAnswer.builder()
                    .senderId(mudKafkaMessage.getId())
                    .senderName(senderName)
                    .recipients(this.recipientsInvestigatorService.getRecipientsId(mudKafkaMessage.getId(), mudKafkaMessage.getGameId()))
                    .message(message)
                    .build();
        }

        if(chatServiceAnswer !=null) {
            this.chatProducer.sendToClientWebsocket(chatServiceAnswer);
        }
    }

    private String getMessageWithoutSender(String[] messageString) {
        StringBuilder builder = new StringBuilder();
        for (int i = 1; i < messageString.length; i++) {
            if (i == messageString.length - 1) {
                builder.append(messageString[i]);
            } else {
                builder.append(messageString[i]).append(":");
            }
        }
        return builder.toString();
    }
}
