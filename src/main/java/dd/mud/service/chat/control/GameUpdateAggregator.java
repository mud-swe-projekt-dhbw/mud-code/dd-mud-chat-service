package dd.mud.service.chat.control;

import dd.mud.service.chat.control.services.ReceiveGameUpdateService;
import dd.mud.service.chat.boundary.model.CompactGame;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class GameUpdateAggregator {

    private final ReceiveGameUpdateService receiveGameUpdateService;

    /**
     * Forwards the CompactGame to the service who is in charge of it.
     * @param compactGame The new game update.
     */
    public void processGameUpdate (CompactGame compactGame) {
        this.receiveGameUpdateService.receiveNewGameUpdate(compactGame);
    }
}
