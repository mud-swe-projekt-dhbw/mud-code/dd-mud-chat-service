package dd.mud.service.chat.control.game;


import dd.mud.service.chat.boundary.model.CompactGame;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class GameCache {

    @Getter
    public Map<Long, CompactGame> cachedGames = new HashMap<>();
}