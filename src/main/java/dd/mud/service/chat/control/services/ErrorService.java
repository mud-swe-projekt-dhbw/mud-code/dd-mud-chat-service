package dd.mud.service.chat.control.services;

import dd.mud.service.chat.boundary.producer.ChatProducer;
import dd.mud.service.chat.boundary.model.ChatServiceAnswer;
import dd.mud.service.chat.boundary.repository.Constants;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ErrorService {

    final ChatProducer chatProducer;
    final RecipientsInvestigatorService recipientsInvestigatorService;

    public void sendErrorMessageToSenderInvalidSyntax(Long senderId, Long currentGame, String senderName){
        ChatServiceAnswer chatErrorAnswer = new ChatServiceAnswer(senderId,
                senderName,
                Collections.emptyList(),
                Constants.ERROR_MESSAGE_INVALID_SYNTAX);

        chatProducer.sendToClientWebsocket(chatErrorAnswer);
    }


    public void sendErrorMessageToSenderUnknownUsername(Long senderId, Long currentGame, String senderName) {
        ChatServiceAnswer chatErrorAnswer = new ChatServiceAnswer(senderId,
                senderName,
                Collections.emptyList(),
                Constants.ERROR_MESSAGE_UNKNOWN_USERNAME);

        chatProducer.sendToClientWebsocket(chatErrorAnswer);
    }
}
