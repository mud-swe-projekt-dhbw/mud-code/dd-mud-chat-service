package dd.mud.service.chat.control.services;

import dd.mud.service.chat.boundary.model.ChatServiceAnswer;
import dd.mud.service.chat.boundary.model.MudKafkaMessage;
import dd.mud.service.chat.boundary.repository.Constants;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class MessageParser {

    private final RecipientsInvestigatorService recipientsInvestigatorService;
    private final ErrorService errorService;

    /**
     * Parses the recipients from the message and returns them as array of string.
     * @param message Message in the following format: @receivername@secondReceiver: message
     * @return An array of string containing all recipients.
     */
    private List<String> parseRecipients(String message) {

        String recipients = message.substring(0, message.indexOf(Constants.COLON));
        recipients = recipients.replace(Constants.WHITESPACE, "");
        List<String> finalRecipientsName = Arrays.asList(recipients.split(Constants.AT));
        return finalRecipientsName.stream().filter(recipient -> !recipient.equals("")).collect(Collectors.toList());
    }

    /**
     * Parses the message and returns it as string.
     * @param message Message in the following format: @receivername@secondReceiver: message
     * @return The message without the recipients.
     */
    private String parseMessage(String message) {
        String chatMessage = message.substring(message.indexOf(Constants.COLON) + 1);
        chatMessage = chatMessage.trim();
        return chatMessage;
    }

    /**
     * Adds DM tag, if player is dungeon master
     * @param playerId The id of the player
     * @param currentGame The game where the player is currently playing
     * @param message The message the player wants to send
     * @return Message with DM Tag, if player is DM, normal message otherwise
     */
    public String addDmTagIfNecessary(Long playerId, Long currentGame, String message){
        if(recipientsInvestigatorService.isRequestFromDungeonMaster(playerId, currentGame)){
            return Constants.DM_TAG.concat(message);
        }
        return message;
    }

    /**
     * Builds the chat answer
     * @param mudKafkaMessage The chat message containing the sender ID and the message.
     * @return An instance of type ChatAnswer.
     */
    public ChatServiceAnswer buildChatMessages(MudKafkaMessage mudKafkaMessage, String senderName) {

        if(!mudKafkaMessage.getMessage().contains(":")){
            errorService.sendErrorMessageToSenderInvalidSyntax(mudKafkaMessage.getId(), mudKafkaMessage.getGameId(), senderName);
            return null;
        }

        List<Long> receiverIds = new ArrayList<>();
        List<String> strings = parseRecipients(mudKafkaMessage.getMessage());
        for (String username : strings) {
            receiverIds.add(recipientsInvestigatorService.getIdByUsername(username, mudKafkaMessage.getGameId()));
        }

        if(receiverIds.contains(null)){
            errorService.sendErrorMessageToSenderUnknownUsername(mudKafkaMessage.getId(), mudKafkaMessage.getGameId(), senderName);
            return null;
        }

        String message = addDmTagIfNecessary(mudKafkaMessage.getId(), mudKafkaMessage.getGameId(),
                parseMessage(mudKafkaMessage.getMessage()));

        return ChatServiceAnswer.builder()
                .senderId(mudKafkaMessage.getId())
                .senderName(senderName)
                .recipients(receiverIds)
                .message(message)
                .build();

    }
}