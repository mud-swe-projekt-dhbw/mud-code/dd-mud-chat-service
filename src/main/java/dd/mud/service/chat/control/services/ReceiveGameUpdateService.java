package dd.mud.service.chat.control.services;

import dd.mud.service.chat.boundary.model.CompactGame;
import dd.mud.service.chat.control.game.GameCache;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ReceiveGameUpdateService {

    private final GameCache gameCache;

    /**
     * Adds the game update to the game cache.
     * @param compactGame The new game update.
     */
    public void receiveNewGameUpdate (CompactGame compactGame) {
        if (this.gameCache.getCachedGames().get(compactGame.getGameId()) == null) {
            this.gameCache.getCachedGames().put(compactGame.getGameId(), compactGame);
        } else {
            this.gameCache.getCachedGames().replace(compactGame.getGameId(), compactGame);
        }
    }
}