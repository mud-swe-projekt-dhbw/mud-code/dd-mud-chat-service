package dd.mud.service.chat.control.services;

import dd.mud.service.chat.boundary.model.CompactGame;
import dd.mud.service.chat.control.game.GameCache;
import dd.mud.service.chat.boundary.repository.Constants;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class RecipientsInvestigatorService {

    private final GameCache gameCache;

    /**
     * Investigates Ids of recipients
     * @param userId Id of message sender
     * @param currentGame Game where the user is located
     * @return list filled
     */
    public List<Long> getRecipientsId(Long userId, Long currentGame){
        if(isRequestFromDungeonMaster(userId, currentGame)){
            return getAllPlayerIdsOfGame(gameCache.getCachedGames().get(currentGame));
        }
        CompactGame.CompactRoom currentRoomOfPlayer = getCurrentRoomOfPlayer(userId, currentGame);
        return getAllPlayerIdsOfRoom(userId, currentRoomOfPlayer);
    }

    /**
     * Gets the id by username
     * @param username Username of investigated player
     * @param currentGame The Game where the player is currently located
     * @return id of player
     */
    public Long getIdByUsername(String username, Long currentGame) {

        if(username.equals(Constants.MESSAGE_FROM_DM)){
            return gameCache.getCachedGames().get(currentGame).getDungeonMasterId();
        }
        for (CompactGame.CompactRoom room : gameCache.getCachedGames().get(currentGame).getRooms()) {
            for(CompactGame.CompactRoom.CompactCharacter character : room.getCharactersInRoom()) {
                if (character.getUserName().equals(username)) {
                    return character.getPlayerId();
                }
            }
        }
        return null;
    }

    /**
     * Retrieves the location of the a player identified by his userId.
     * @param userId The userId of the player to retrieve his location from.
     * @param currentGame The gameId of which the room must be retrieved.
     * @return The current room where the player is located.
     */
    private CompactGame.CompactRoom getCurrentRoomOfPlayer(Long userId, Long currentGame){

        for (CompactGame.CompactRoom room : gameCache.getCachedGames().get(currentGame).getRooms()) {
            if (isPlayerInRoom(userId, room)) {
                return room;
            }
        }
        return null;
    }

    /**
     * Checks if a user is a dungeon master.
     * @param userId The users userId
     * @param currentGame The game where the user is located.
     * @return True when the user is a dungeon master, otherwise false.
     */
    public boolean isRequestFromDungeonMaster(Long userId, Long currentGame) {
        System.out.println(userId + " " + currentGame + " " + gameCache.getCachedGames().get(currentGame));
        return gameCache.getCachedGames().get(currentGame).getDungeonMasterId().equals(userId);
    }

    /**
     * Retrieves all players of a game.
     * @param currentGame The game to retrieve the players from.
     * @return A list of long with all the player Ids of a game.
     */
    private List<Long> getAllPlayerIdsOfGame(CompactGame currentGame) {
        List<Long> playerIdsOfGame = new ArrayList<>();
        for (CompactGame.CompactRoom room : currentGame.getRooms()) {
            for (CompactGame.CompactRoom.CompactCharacter character : room.getCharactersInRoom()){
                playerIdsOfGame.add(character.getPlayerId());
            }
        }
        return playerIdsOfGame;
    }

    /**
     * Retrieves all players of a room.
     * @param userId The userId of the sender.
     * @param currentRoom The room to retrieve the players from.
     * @return A list of long with all the player Ids of a room.
     */
    private List<Long> getAllPlayerIdsOfRoom(Long userId, CompactGame.CompactRoom currentRoom) {
        if(currentRoom != null) {
            List<Long> allPlayerIdsOfRoom = currentRoom.getCharactersInRoom().stream()
                    .map(CompactGame.CompactRoom.CompactCharacter::getPlayerId)
                    .collect(Collectors.toList());
            allPlayerIdsOfRoom.remove(userId);
            return allPlayerIdsOfRoom;
        }
        return Collections.emptyList();
    }

    /**
     * Checks if a player is in a particular room.
     * @param userId The player identified by his userId
     * @param room The room to check whether the player is in or not.
     * @return True when the player is in the room, otherwise false.
     */
    private boolean isPlayerInRoom(Long userId, CompactGame.CompactRoom room) {

        for (CompactGame.CompactRoom.CompactCharacter character : room.getCharactersInRoom()) {
            if(character.getPlayerId().equals(userId)){
                return true;
            }
        }
        return false;

    }
}