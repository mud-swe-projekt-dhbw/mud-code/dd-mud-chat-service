package dd.mud.service.chat.boundary.consumer;

import dd.mud.service.chat.control.ChatMessageAggregator;
import dd.mud.service.chat.boundary.model.MudKafkaMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ChatConsumerTest_F150 {

    @InjectMocks
    ChatConsumer chatConsumer;

    @Mock
    ChatMessageAggregator chatMessageAggregator;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void receiveChatMessage() {

        MudKafkaMessage mudKafkaMessage = new MudKafkaMessage();

        chatConsumer.receiveChatMessage(mudKafkaMessage);

        verify(chatMessageAggregator, times(1)).processChatMessage(mudKafkaMessage);
    }
}