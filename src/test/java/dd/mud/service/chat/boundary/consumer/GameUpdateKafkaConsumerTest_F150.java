package dd.mud.service.chat.boundary.consumer;

import dd.mud.service.chat.control.GameUpdateAggregator;
import dd.mud.service.chat.boundary.model.CompactGame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class GameUpdateKafkaConsumerTest_F150 {

    @InjectMocks
    GameUpdateKafkaConsumer gameUpdateKafkaConsumer;

    @Mock
    GameUpdateAggregator gameUpdateAggregator;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getCurrentGameFromGameService() {
        CompactGame compactGame = new CompactGame();

        gameUpdateKafkaConsumer.getCurrentGameFromGameService(compactGame);

        verify(gameUpdateAggregator, times(1)).processGameUpdate(compactGame);
    }
}