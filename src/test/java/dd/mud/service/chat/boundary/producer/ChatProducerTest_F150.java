package dd.mud.service.chat.boundary.producer;

import dd.mud.service.chat.boundary.model.ChatServiceAnswer;
import dd.mud.service.chat.boundary.repository.KafkaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.kafka.core.KafkaTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class ChatProducerTest_F150 {

    @InjectMocks
    ChatProducer chatProducer;

    @Mock
    KafkaTemplate<String, ChatServiceAnswer> kafkaTemplate;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void sendToClientWebsocket() {

        ChatServiceAnswer chatServiceAnswer = new ChatServiceAnswer();
        final String SEND_TO_CWS_TOPIC = "chatAnswer.t";

        chatProducer.sendToClientWebsocket(chatServiceAnswer);

        verify(kafkaTemplate, times(1)).send(SEND_TO_CWS_TOPIC, chatServiceAnswer);

    }
}