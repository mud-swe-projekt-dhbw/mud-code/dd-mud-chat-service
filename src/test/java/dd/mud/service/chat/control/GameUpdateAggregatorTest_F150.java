package dd.mud.service.chat.control;

import dd.mud.service.chat.control.services.ReceiveGameUpdateService;
import dd.mud.service.chat.boundary.model.CompactGame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class GameUpdateAggregatorTest_F150 {

    @InjectMocks
    GameUpdateAggregator gameUpdateAggregator;

    @Mock
    ReceiveGameUpdateService receiveGameUpdateService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void processGameUpdate() {
        CompactGame compactGame = new CompactGame();
        gameUpdateAggregator.processGameUpdate(compactGame);
        verify(receiveGameUpdateService, times(1)).receiveNewGameUpdate(any());
    }
}