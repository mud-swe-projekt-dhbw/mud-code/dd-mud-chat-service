package dd.mud.service.chat.control;

import dd.mud.service.chat.boundary.producer.ChatProducer;
import dd.mud.service.chat.control.services.MessageParser;
import dd.mud.service.chat.control.services.RecipientsInvestigatorService;
import dd.mud.service.chat.boundary.model.ChatServiceAnswer;
import dd.mud.service.chat.boundary.model.MudKafkaMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


class MudKafkaMessageAggregatorTest_F150 {

    @InjectMocks
    ChatMessageAggregator chatMessageAggregator;

    @Mock
    RecipientsInvestigatorService recipientsInvestigatorService;

    @Mock
    MessageParser messageParser;

    @Mock
    ChatProducer chatProducer;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void processChatMessage() {
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder()
                .id(0L)
                .message("Sender: message")
                .gameId(0L)
                .build();

        ChatServiceAnswer chatServiceAnswer = ChatServiceAnswer.builder().senderId(0L)
                .senderName("user")
                .message("Sender: message")
                .build();

        chatMessageAggregator.processChatMessage(mudKafkaMessage);
        when(messageParser.buildChatMessages(any(), any())).thenReturn(chatServiceAnswer);

        verify(chatProducer, times(1)).sendToClientWebsocket(any());
    }
}