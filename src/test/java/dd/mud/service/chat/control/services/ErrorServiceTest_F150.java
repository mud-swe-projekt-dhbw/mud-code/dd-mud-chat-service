package dd.mud.service.chat.control.services;

import dd.mud.service.chat.boundary.producer.ChatProducer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ErrorServiceTest_F150 {

    @InjectMocks
    ErrorService errorService;

    @Mock
    ChatProducer chatProducer;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void sendErrorMessageToSenderInvalidSyntax() {
        Long senderId = 0L;
        String senderUsername = "Test";
        Long currentGame = 0L;

        errorService.sendErrorMessageToSenderInvalidSyntax(senderId, currentGame, senderUsername);

        verify(chatProducer, times(1)).sendToClientWebsocket(any());
    }

    @Test
    void sendErrorMessageToSenderUnknownUsername() {
        Long senderId = 0L;
        String senderUsername = "Test";
        Long currentGame = 0L;

        errorService.sendErrorMessageToSenderUnknownUsername(senderId, currentGame, senderUsername);

        verify(chatProducer, times(1)).sendToClientWebsocket(any());
    }
}