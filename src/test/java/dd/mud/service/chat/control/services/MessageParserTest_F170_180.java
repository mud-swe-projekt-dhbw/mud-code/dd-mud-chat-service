package dd.mud.service.chat.control.services;

import dd.mud.service.chat.boundary.model.ChatServiceAnswer;
import dd.mud.service.chat.boundary.model.MudKafkaMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class MessageParserTest_F170_180 {

    @InjectMocks
    MessageParser messageParser;

    @Mock
    RecipientsInvestigatorService recipientsInvestigatorService;

    @Mock
    ErrorService errorService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void buildChatMessages() {
        Long id = 1L;
        Long gameId = 2L;
        String message = "@receiverOne@receiverTwo: message";
        String senderName = "user";
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder()
                .id(id)
                .gameId(gameId)
                .message(message)
                .build();

        List<Long> recipients = new ArrayList<Long>();
        recipients.add(0L);
        recipients.add(0L);

        ChatServiceAnswer expectedChatServiceAnswer = ChatServiceAnswer.builder()
                .senderId(1L)
                .senderName(senderName)
                .message("message")
                .recipients(recipients)
                .build();

        when(recipientsInvestigatorService.isRequestFromDungeonMaster(id, gameId)).thenReturn(false);

        ChatServiceAnswer actualChatServiceAnswer = messageParser.buildChatMessages(mudKafkaMessage, senderName);

        assertEquals(actualChatServiceAnswer, expectedChatServiceAnswer);
    }

    @Test
    void buildChatMessagesInvalidSyntax() {
        Long id = 1L;
        Long gameId = 2L;
        String message = "@receiverOne@receiverTwo message";
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder()
                .id(id)
                .gameId(gameId)
                .message(message)
                .build();

        ChatServiceAnswer actualChatServiceAnswer = messageParser.buildChatMessages(mudKafkaMessage, "username");

        verify(errorService, times(1)).sendErrorMessageToSenderInvalidSyntax(any(), any(), anyString());
        assertNull(actualChatServiceAnswer);
    }

    @Test
    void buildChatMessageUnknownUsername() {
        Long id = 1L;
        Long gameId = 2L;
        String message = "@unknownUsername: message";
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder()
                .id(id)
                .gameId(gameId)
                .message(message)
                .build();

        when(recipientsInvestigatorService.getIdByUsername(any(), any())).thenReturn(null);

        ChatServiceAnswer actualChatServiceAnswer = messageParser.buildChatMessages(mudKafkaMessage, "username");

        verify(errorService, times(1)).sendErrorMessageToSenderUnknownUsername(any(), any(), anyString());
        assertNull(actualChatServiceAnswer);
    }
}