package dd.mud.service.chat.control.services;

import dd.mud.service.chat.control.game.GameCache;
import dd.mud.service.chat.boundary.model.CompactGame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


class RecipientsInvestigatorServiceTest_F150 {

    RecipientsInvestigatorService recipientsInvestigatorService;
    GameCache gameCache;

    CompactGame compactGame0;
    Long keyGame0 = 0L;
    Long idGame0 = 0L;
    Long dmIdGame0 = 42L;
    CompactGame.CompactRoom room0game0;
    CompactGame.CompactRoom.CompactCharacter character0;
    CompactGame.CompactRoom.CompactCharacter character1;
    Long characterId0Game0 = 0L;
    Long characterId1Game0 = 1L;
    String usernameCharacter0Game0 = "Test0";
    String usernameCharacter1Game0 = "Test1";
    CompactGame.CompactRoom room1game0;

    @BeforeEach
    public void init(){

        gameCache = new GameCache();
        compactGame0 = CompactGame.builder().build();

        // Set DM
        compactGame0.setDungeonMasterId(dmIdGame0);

        //Set Room 0 with character
        room0game0 = new CompactGame.CompactRoom();
        room0game0.setCharactersInRoom(Collections.emptyList());

        //Set Room 1 with character
        room1game0 = new CompactGame.CompactRoom();
        character0 = new CompactGame.CompactRoom.CompactCharacter();
        character1 = new CompactGame.CompactRoom.CompactCharacter();
        character0.setPlayerId(characterId0Game0);
        character1.setPlayerId(characterId1Game0);
        character0.setUserName(usernameCharacter0Game0);
        character1.setUserName(usernameCharacter1Game0);
        room1game0.setCharactersInRoom(Arrays.asList(character0, character1));

        //Add rooms to compactGame und gameCache
        compactGame0.setRooms(Arrays.asList(room0game0, room1game0));
        compactGame0.setGameId(idGame0);
        gameCache.cachedGames.put(keyGame0, compactGame0);

        recipientsInvestigatorService = new RecipientsInvestigatorService(gameCache);

    }

    @Test
    void getRecipientsIdDmCase_F180() {
        List<Long> expectedResult = new ArrayList<Long>();
        expectedResult.add(characterId0Game0);
        expectedResult.add(characterId1Game0);

        List<Long> actualResult =
                recipientsInvestigatorService.getRecipientsId(dmIdGame0, compactGame0.getGameId());

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void getRecipientsIdPlayerCase_F160() {
        List<Long> expectedResult = new ArrayList<Long>();
        expectedResult.add(characterId1Game0);

        List<Long> actualResult =
                recipientsInvestigatorService.getRecipientsId(characterId0Game0, compactGame0.getGameId());

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void getRecipientsIdInvalidUnknownUserId() {
        Long unknownUserId = 999L;
        List<Long> expectedResult = Collections.emptyList();

        List<Long> actualResult =
                recipientsInvestigatorService.getRecipientsId(unknownUserId, compactGame0.getGameId());

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void getIdByUsernameOfPlayerSuccessful() {
        assertEquals(characterId0Game0,
                recipientsInvestigatorService.getIdByUsername(usernameCharacter0Game0, idGame0));
        assertEquals(characterId1Game0,
                recipientsInvestigatorService.getIdByUsername(usernameCharacter1Game0, idGame0));
    }

    @Test
    void getIdByUsernameOfDmSuccessful() {
        String messageFromDm = "DM";
        assertEquals(dmIdGame0,
                recipientsInvestigatorService.getIdByUsername(messageFromDm, idGame0));
    }

    @Test
    void getIdByUsernameFailed() {
        String unknownUsername = "UnknownTester";
        assertNull(recipientsInvestigatorService.getIdByUsername(unknownUsername, idGame0));
    }

    @Test
    void isRequestFromDungeonMasterTrue_F160_F180() {
        assertTrue(recipientsInvestigatorService.isRequestFromDungeonMaster(dmIdGame0, idGame0));
    }

    @Test
    void isRequestFromDungeonMasterFalse_F160_F180() {
        assertFalse(recipientsInvestigatorService.isRequestFromDungeonMaster(characterId0Game0, idGame0));
        assertFalse(recipientsInvestigatorService.isRequestFromDungeonMaster(null, idGame0));
    }
}